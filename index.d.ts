import { Middleware } from 'koa';

declare function koa2Swagger(config: koa2Swagger.SwaggerOptions): Middleware;

declare namespace koa2Swagger {
    interface SwaggerOptions {
        title?: string;
        oauthOptions?: any;
        swaggerOptions?: {
            dom_id?: string;
            url?: string;
            supportedSubmitMethods?: ['get', 'post', 'put', 'delete', 'patch'];
            spec?: any;
            docExpansion?: 'list' | 'full' | 'none';
            defaultModelRendering?: 'schema';
            showRequestHeaders?: boolean;
            swaggerVersion?: string;
        }
        routePrefix?: string;
        hideTopbar?: boolean;
    }
}

export = koa2Swagger;
